package br.com.lp3.apifornecedor.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.lp3.apifornecedor.dto.FornecedorDTO;
import br.com.lp3.apifornecedor.service.FornecedorService;

public class ProdutoController {

	public static void main(String[] args) private ProdutoService ProdutoService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProdutoDTO> findClienteById(@PathVariable Long id) {
		ProdutoDTO ProdutoDTO = ProdutoService.findById(id);
		if (ProdutoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(ProdutoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProdutoDTO>(ProdutoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<ProdutoDTO> saveProduto(@RequestBody ProdutoDTO ProdutoDTO) {
		ProdutoDTO = ProdutoService.saveProduto(ProdutoDTO);
		if (ProdutoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(ProdutoDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<ProdutoDTO>(ProdutoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}