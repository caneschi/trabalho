package br.com.lp3.apifornecedor.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.lp3.apifornecedor.model.Fornecedor;

@Repository
public interface FornecedorRepository

	Fornecedor save(Fornecedor fornecedor); extends CrudRepository<Fornecedor, int>{

}

	Optional<Fornecedor> findById(int id);
