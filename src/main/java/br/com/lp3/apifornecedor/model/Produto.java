package br.com.lp3.apifornecedor.model;

public class Produto {

	private int id;
	
	private String nome;

	private String endereco;

	private String Descricao;

	private double valor;
	

	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEndereco() {
		return endereco;
	}


	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getId() {
		return id;
	}
	public  Produto(int id, String nome, String Descricao, double valor) {
		super();
		this.id = id;
		this.nome = nome;
		this.setDescricao(Descricao);
		this.setValor(valor);
	}

	public  Produto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getDescricao() {
		return Descricao;
	}


	public void setDescricao(String descricao) {
		Descricao = descricao;
	}


	public double getValor() {
		return valor;
	}


	public void setValor(double valor) {
		this.valor = valor;
	}
}
