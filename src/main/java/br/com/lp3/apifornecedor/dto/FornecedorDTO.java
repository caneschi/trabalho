package br.com.lp3.apifornecedor.dto;

public class FornecedorDTO {

	private int id;
	
	private String nome;

	private String endereco;
	
	private long cnpj;
	
	private String razao_social;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public FornecedorDTO(int id, String nome, String endereco, long cnpj, String razao_social) {
		super();
		this.id = id;
		this.nome = nome;
		this.endereco = endereco;
		this.cnpj = cnpj;
		this.razao_social = razao_social;
	}

	public FornecedorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	
}
