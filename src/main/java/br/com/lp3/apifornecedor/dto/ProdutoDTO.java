package br.com.lp3.apifornecedor.dto;

public class ProdutoDTO {
	private int id;
	private String nome;
	private String Descricao;
	private double valor;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public ProdutoDTO(int id, String nome, String Descricao, double valor) {
		super();
		this.id = id;
		this.nome = nome;
		this.Descricao = Descricao;
		this.valor = valor;
	}

	public ProdutoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
